package org.eclipse.ckeditor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.ckeditor.preference.ToolbarPrefs;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.preference.IPreferenceStore;
import org.osgi.framework.Bundle;

import fi.iki.elonen.NanoHTTPD;

/**
 * Embedded web server for manage resources for CKEditor.
 *  
 * @author Konstantin Zaitcev
 */
public class WebServer extends NanoHTTPD {
	/** Reference to bundle. */
	private final Bundle bundle;

	public WebServer(int port, Bundle bundle) {
		super(port);
		this.bundle = bundle;
	}

	@Override
	public Response serve(String uri, Method method,
			Map<String, String> headers, Map<String, String> parms,
			Map<String, String> files) {
		// request our generated config
		if ("/_plugin_/config.js".equals(uri)) {
			return new Response(getConfigParams());
		}
		
		// request file to edit
		if (parms.containsKey("file")) {
			return getEditorFile(parms.get("file"));

		// request project relative resources
		} else if (uri.startsWith("/_ws_")) {
			return getRelativeProjectResource(uri.substring(5));
		}
		URL resource = bundle.getResource(uri);
		if (resource != null) {
			try {
				return new Response(Response.Status.OK, getMimeType(uri), resource.openStream());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		// in other case display debug info
		StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head><title>Debug Server</title></head>");
        sb.append("<body>");
        sb.append("<h1>Response</h1>");
        sb.append("<p><blockquote><b>URI -</b> ").append(uri).append("<br />");
        sb.append("<b>Method -</b> ").append(method).append("</blockquote></p>");
        sb.append("<h3>Headers</h3><p><blockquote>").append(headers).append("</blockquote></p>");
        sb.append("<h3>Parms</h3><p><blockquote>").append(parms).append("</blockquote></p>");
        sb.append("<h3>Files</h3><p><blockquote>").append(files).append("</blockquote></p>");
        sb.append("</body>");
        sb.append("</html>");
        return new Response(sb.toString());
	}
	
	private Response getRelativeProjectResource(String uri) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();  
		IFile file = workspace.getRoot().getFile(new Path(uri));  
		try {
			return new Response(Response.Status.OK, getMimeType(uri), file.getContents());
		} catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}

	private Response getEditorFile(String f) {
		try {
			URL resource = bundle.getResource("/template/editor_template.html");
			String template = Utils.getStringFromStream(resource.openStream());
			IWorkspace workspace = ResourcesPlugin.getWorkspace();  
			IFile file = workspace.getRoot().getFile(new Path(f));
			if (file.exists()) {
				String content = Utils.getStringFromStream(file.getContents(), file.getCharset());
				template = template.replace("#TIMESTAMP#", String.valueOf(System.currentTimeMillis()));
				template = template.replace("#FILE#", content);
			}
			
			return new Response(Response.Status.OK, "text/html", template);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String getConfigParams() {
		IPreferenceStore pref = Activator.getDefault().getPreferenceStore();
		if (pref.getBoolean("config_file_enable")) {
			return getConfigFromFile(pref.getString("config_file"));
		} else {
			return getConfigFromPref(pref);
		}
	}

	private String getConfigFromPref(IPreferenceStore pref) {
		StringBuilder sb = new StringBuilder();
		sb.append("CKEDITOR.editorConfig = function( config ) {\n");
		sb.append("\tconfig.language = '" + pref.getString("language") + "';\n");
		sb.append("\tconfig.toolbar = [\n");

		StringBuilder tb = new StringBuilder();
		for (Entry<String, String> entry: ToolbarPrefs.TOOLBAR.entrySet()) {
			if (pref.getBoolean("toolbar_" + entry.getKey())) {
				if (tb.length() > 0) {
					tb.append(",");
				}
				tb.append("\t{ name: '" + entry.getKey() + "',\t items: [" + entry.getValue() + "]}");
			}
		}
		sb.append(tb.toString());
		sb.append("];");
		sb.append("};\n");
		return sb.toString();
	}

	private String getConfigFromFile(String file) {
		try {
			return Utils.getStringFromStream(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static String getMimeType(String uri) {

		if (uri.endsWith(".html") || uri.endsWith(".htm")) {
			return "text/html";
		}

		if (uri.endsWith(".jpg") || uri.endsWith(".jpeg")) {
			return "image/jpeg";
		}

		if (uri.endsWith(".png")) {
			return "image/png";
		}

		if (uri.endsWith(".gif")) {
			return "image/gif";
		}

		if (uri.endsWith(".js")) {
			return "application/javascript";
		}
		
		if (uri.endsWith(".css")) {
			return "text/css";
		}
		
		
		return "text/plain";
	}
}
